include .env

default: up

## up	:	Start up containers.
.PHONY: up
up:
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose pull
	docker-compose up -d --remove-orphans

## shell	:	Access `nextjs` container via shell.
##		You can optionally pass an argument with a service name to open a shell on the specified container
.PHONY: shell
shell:
	docker exec -ti -e COLUMNS=$(shell tput cols) -e LINES=$(shell tput lines) $(shell docker ps --filter name='$(PROJECT_NAME)_$(or $(filter-out $@,$(MAKECMDGOALS)), 'nextjs')' --format "{{ .ID }}") sh

## npm	:	Executes `npm` command in nextjs container.
##		To use "--flag" arguments include them in quotation marks.
##		For example: make npm "install --help"
.PHONY: npm
npm:
	docker exec $(shell docker ps --filter name='^/$(PROJECT_NAME)_nextjs' --format "{{ .ID }}") npm $(filter-out $@,$(MAKECMDGOALS))

## logs	:	View containers logs.
##		You can optionally pass an argument with the service name to limit logs
##		logs php	: View `php` container logs.
##		logs nginx php	: View `nginx` and `php` containers logs.
.PHONY: logs
logs:
	@docker-compose logs -f $(filter-out $@,$(MAKECMDGOALS))

# https://stackoverflow.com/a/6273809/1826109
%:
	@:
